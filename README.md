# Práctica de sintaxis de plantillas de Angular.

## Puesta en marcha del proyecto

* Crear carpeta para el nuevo proyecto.
* Abrir carpeta con VSCode.
* Abrir el terminal con **ctrl+ñ**
* Ejecutar estos comandos:
````
git clone http://sgit.emuasa.es/emuasa/practica-template-syntax.git .
npm install
npm start
````
* Eliminar la carpeta **.git** para desvicular este repositorio.

## Ejercicio a realizar

### 1. Mostrar personas en pantalla

1. Injectar el servicio `PeopleService` en `AppComponent` y cargar la lista de personas usando `PeopleService.getPeople()`.
2. Cambiar la plantilla para mostrar en pantalla, en una tabla, las personas recuperadas del servicio, mostrar al menos los campos `name`, `gender`, `age` y `balance`.
3. Cuando tengamos las personas en pantalla vamos a crear un nuevo componente `PersonRowComponent` en el fichero `person-row.component.ts`, este nuevo componente mostrará la información de una persona en la tabla, haremos que por cada persona `AppComponent` use un `PersonRowComponent` para mostrar cada linea de la tabla.
4. Aprovechar que tenemos bootstrap para dar estilo a la tabla añadiendo las clases `table table-striped table-hover`

### 2. Editar personas

1. Prepararemos `PersonRowComponent` para añadir un botón "Editar" en la columna de la derecha. Ese botón debe emitir un evento, `onEditClick` que debe ser capturado por `AppComponent` y este último debe llamar al servicio para cargar la persona a editar y guardarla en una propiedad `editingPerson`.
2. Crearemos un componente, `PersonEditComponent` -> `person-edit.component`, para poder editar una persona, como mínimo podremos editar los mismos campos que se ven en la tabla más un `<textarea>` para el campo `about`. La persona a editar viene como propiedae de entrada al componente.
3. En `AppComponent`, debajo de la tabla, pondremos nuestro componente `PersonEditComponent` al que le pasaremos `editingPerson` para indicarle que persona debe editar.
4. `PersonEditComponent` debe tener un botón para cancelar la edición y otro para aceptar, estos botones emitirán sus correspondientes eventos `onCancelEditclick`, `onOkEditclick`, los cuales deben ser tratados en `AppComponent`, uno ocultando la edición y el otro llamando al servicio para guardar.

## 3. Crear Personas, paginación.

1. Debajo del grid mostraremos un botón para poder añadir una nueva persona, aprovecharemos el mismo componente de edición también para añadir.
    * Cuando estamos creando, los botones de editar del grid deben estar desactivados.
    * Cuando se está editando... no se puede crear.
    * El título de `PersonEditComponent` debe ser distinto cuando se edita y cuando se crea.
2. Podemos observar que cuando creamos una persona el grid no la muestra, eso es por que el servicio carga las personas por páginas, por defecto el método `PeopleService.getPeople` carga las 5 primeras personas, debemos hacer `AppComponent` pueda cambar de página.
    * Crearemos una propiedad llamada `page` que se inicializará a 0. Y otra que indique cuantos registros mostraremos por página `recordsPerPage` que inicializaremos a 4.
    * Condicionaremos el metodo `getList` de `AppComponent` para que pida los registros según la variable `page`.
    * Pondremos un botón para ir a la siguiente página y otro para ir a la anterior.
3. En `PersonRowComponent` mostrar en un `<img>` la propiedad `picture` de cada persona. Como cuando creamos no pasamos dicha propiedad, al mostrar los nuevos elementos obtendremos un error en la consola por cada fila que no lleve `picture`. Solventar esa casuistica.

## 4. Extras

1. En la cabecera del grid, en la columna del botón editar pondremos un botón para cambiar el estilo de la tabla (Añadiendo o quitando clases bootstrap).
2. Crearemos un componente para editar el género de la persona. Creamos un `GenderEditorComponent`, este componente mostrará dos checkboxes, uno para Hombre y otro para Mujer, el componente tomará como entrada una propiedad `value` donde irá el valor inicial, cuando pinchemos en uno u otro checkbox emitirá un evento para comunicar del cambio.
3. Cuando llamamos al servicio, para cargar las personas, para guardar o para insertar, hay un retardo. Debemos de habilitar `AppComponent` para que cuando esté el servicio trabajando no podamos realizar acciones que dependan del trabajo que se está realizando.
