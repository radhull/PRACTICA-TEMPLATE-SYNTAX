import { Component } from "@angular/core";

@Component({
  moduleId: module.id,
  selector: "my-app",
  template: `<my-header></my-header>
  <router-outlet></router-outlet>
  `
}
)
export class AppComponent {
}
