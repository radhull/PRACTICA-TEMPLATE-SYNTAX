import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";

import { HeaderModule } from "./header";
import { PersonModule } from "./person";

import { AppComponent } from "./app.component";

@NgModule({
    imports: [BrowserModule, HeaderModule, PersonModule, RouterModule.forRoot([])],
    declarations: [AppComponent],
    bootstrap: [AppComponent]
})
export class AppModule { }
