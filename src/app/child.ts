import { Component, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { ModalDirective } from "ng2-bootstrap";

@Component({
  moduleId: module.id,
  selector: "demo-modal-child",
  templateUrl: "child.html",
  exportAs: "child"
})
export class DemoModalChildComponent {
  @ViewChild("childModal") public childModal: ModalDirective;

  @Input()
  modal = {
    "header": "MODAL HEADER",
    "text": "MODAL TEXT",
    "txtAceptar": "Eliminar",
    "txtCancelar": "Cancelar"
  };
  @Output()
  ok: EventEmitter<any> = new EventEmitter();


  public showChildModal(): void {
    this.childModal.show();
  }

  public hideChildModal(): void {
    this.childModal.hide();
    this.ok.emit(false);
  }

  public confirChildModal(): void {
    this.childModal.hide();
    this.ok.emit(true);
  }
}
