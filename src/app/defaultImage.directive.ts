import { Directive, Input } from "@angular/core";

@Directive({
    selector: "img[src]",
    host: {
        "[src]": "checkPath(src)",
        "(error)": "onError()"
    }
})
export class DefaultImage {
    @Input() src: string;
    public defaultImg: string = "http://placehold.it/32x32g";
    public onError() {
        return this.defaultImg;
    }
    public checkPath(src: string) {
        return src ? src : this.defaultImg;
    }
}
