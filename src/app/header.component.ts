import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";


@Component({
    moduleId: module.id,
    selector: "my-header",
    templateUrl: "header.component.html",
})
export class HeaderComponent implements OnInit {
    name = "Mantenimiento de personas";

    constructor(private titleService: Title) {
    }


    setTitle(newTitle: string) {
        this.titleService.setTitle(newTitle);
    }

    ngOnInit(): void {
        this.setTitle(this.name);
    }


}
