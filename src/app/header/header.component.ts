import { Component } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "my-header",
    templateUrl: "header.component.html",
})
export class HeaderComponent {
    name = "Mantenimiento de personas";
}
