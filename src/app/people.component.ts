import { Component, OnInit } from "@angular/core";
import { Title } from "@angular/platform-browser";

import { Person, ReducedPerson } from "./person";
import { PeopleService } from "./people.service";


@Component({
  moduleId: module.id,
  selector: "my-people",
  templateUrl: "people.component.html",
  styles: [`.thead-inverse th {  color: #fff;  background-color: #373a3c;}    
            .tdVacio td{height:48px}
            .pager{margin-top: 0px;
            margin-bottom:0px}`],
  providers: [PeopleService]
})
export class PeopleComponent implements OnInit {
  name = "Mantenimiento de personas";
  people: ReducedPerson[];

  editingPerson: Person;
  creatingPerson: Person;

  page: number;
  recordsPerPage: number;

  quitarClases: boolean;
  currentClasses: {};

  trabajando: boolean;

  counter = Array;

  constructor(private peopleService: PeopleService, private titleService: Title) {
  }

  setCurrentClasses() {
    this.currentClasses = {
      "table table-striped table-bordered table-hover": this.quitarClases
    };
  }

  cambiarClases() {
    this.quitarClases = !this.quitarClases;
    this.setCurrentClasses();
  }


  setTitle(newTitle: string) {
    this.titleService.setTitle(newTitle);
  }

  ngOnInit(): void {
    this.setTitle(this.name);
    this.cambiarClases();
    this.page = 0;
    this.people = [] as Person[];
    this.recordsPerPage = 4;
    this.trabajando = true;
    this.getList();
  }

  onAddclick(): void {
    this.vaciarEdicion();
    this.creatingPerson = {
      gender: "male"
    } as Person;
    this.trabajando = false;
  }

  getList(): void {
    this.peopleService.getPeople((this.page * this.recordsPerPage), this.recordsPerPage)
      .then(people => {
        if (people.length === 0 && this.page > 0) {
          this.page--;
          this.getList();
        } else {
          this.people = people;
          this.trabajando = false;
        }
      })
      .catch((err) => console.error(err));

  }

  onEdit(person: ReducedPerson): void {
    this.vaciarEdicion();
    this.peopleService.getPerson(person._id).then(p => {
      this.editingPerson = p;
      this.trabajando = false;
    })
      .catch((err) => console.error(err));
    ;
  }

  onDel(person: Person): void {
    this.vaciarEdicion();
    this.peopleService.deletePerson(person).then(() => this.getList())
      .catch((err) => console.error(err));
  }

  onEditOk(person: Person): void {
    this.vaciarEdicion();
    this.peopleService.savePerson(person)
      .then(() => this.getList())
      .catch((err) => console.error(err));

  }

  onCancel(): void {
    this.vaciarEdicion();
    this.trabajando = false;
  }



  onNewOk(person: Person): void {
    this.vaciarEdicion();
    this.peopleService.insertPerson(person).then(() => this.getList()).catch((err) => console.error(err));
  }

  private vaciarEdicion(): void {
    this.trabajando = true;
    this.editingPerson = null;
    this.creatingPerson = null;
  }

  onNextClick(event?: MouseEvent): void {
    if (event) {
      event.preventDefault();
    }

    if (!(this.people.length !== this.recordsPerPage || this.trabajando)) {
      this.vaciarEdicion();
      this.page++;
      this.getList();
    }
  }
  onPrevClick(event?: MouseEvent): void {
    if (event) {
      event.preventDefault();
    }
    if (!(this.page <= 0 || this.trabajando)) {
      this.vaciarEdicion();
      this.page--;
      this.getList();
    }
  }

}
