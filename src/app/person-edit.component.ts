import { Component, Input, Output, EventEmitter } from "@angular/core";
import { NgForm } from "@angular/forms";

import { Person } from "./person";

@Component({
    moduleId: module.id,
    selector: "person-edit",
    templateUrl: "person-edit.component.html",
    styles: [`
            .ng-valid[required], .ng-valid.required { border-left: 5px solid #42A948; /* green */ }
            .ng-invalid:not(form) { border-left: 5px solid #a94442; /* red */}
            .error{padding: 12px;
                    background-color: rgba(255, 0, 0, 0.2);
                    color: red;
                   }       
            `]
})
export class PersonEditComponent {
    @Input()
    obj: Person;

    @Output()
    save: EventEmitter<Person> = new EventEmitter();

    @Output()
    cancel: EventEmitter<any> = new EventEmitter();

    // personForm: FormGroup;

    // constructor(fb: FormBuilder) {
    //     this.personForm = fb.group({
    //         "person_name": [null, Validators.required],
    //         "person_age": [null, Validators.minLength(2)],
    //         "person_balance": [null, Validators.minLength(10)],
    //         "person_about": [null, null]
    //     });
    // }
    onSaveclick(personForm: NgForm): void {
        if (personForm.valid) {
            this.save.emit(this.obj);

        } else {
            console.log("persona form no valid");
        }
    }
    onCancelclick(): void {
        this.cancel.emit(null);
    }

}


