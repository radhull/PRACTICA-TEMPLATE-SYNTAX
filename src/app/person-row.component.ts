import { Component, Input, Output, EventEmitter } from "@angular/core";


import { ReducedPerson } from "./person";

@Component({
    moduleId: module.id,
    selector: "tr [person-row]",
    templateUrl: "person-row.component.html",
    styleUrls: ["style.tpl.css"]
})
export class PersonRowComponent {

    @Input()
    obj: ReducedPerson;

    @Input()
    bEnAlta: boolean;

    @Input()
    trabajando: boolean;

    @Output()
    edit: EventEmitter<ReducedPerson> = new EventEmitter();

    @Output()
    del: EventEmitter<ReducedPerson> = new EventEmitter();

    modalDel = {
        "header": "Atención",
        "text": "¿Desea eliminar la persona?",
        "txtAceptar": "Eliminar",
        "txtCancelar": "Cancelar"
    };

    onEditClick(): void {
        this.edit.emit(this.obj);
    }

    onDelClick(): void {
        this.del.emit(this.obj);
    }
    onDelete(ok: boolean): void {
        if (ok) {
            this.del.emit(this.obj);
        }
    }

}
