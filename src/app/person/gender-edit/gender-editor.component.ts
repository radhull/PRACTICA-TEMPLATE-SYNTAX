import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
    moduleId: module.id,
    selector: "gender-editor",
    templateUrl: "gender-editor.component.html"
})
export class GenderEditorComponent {

    @Input()
    gender: string;

    @Output()
    genderChange: EventEmitter<any> = new EventEmitter();

    onClick(gender: string): void {
        this.gender = gender;
        this.genderChange.emit(this.gender);
    }

}
