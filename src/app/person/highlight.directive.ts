import { Directive, ElementRef, HostListener, Input } from "@angular/core";
@Directive({
    selector: "[highlight]"
})
export class HighlightDirective {

    private _defaultColor = "beige";
    private el: HTMLElement;

    @Input("highlight")
    highlightColor: string;

    constructor(el: ElementRef) {
        this.el = el.nativeElement;
    }



    @HostListener("mouseenter")
    onMouseEnter(): void {
        this.highlight(this.highlightColor || this._defaultColor);
    }
    @HostListener("mouseleave")
    onMouseLeave(): void {
        this.highlight(null);
    }

    private highlight(color: string) {
        this.el.style.backgroundColor = color;
    }
}