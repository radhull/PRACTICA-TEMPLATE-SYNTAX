import { Component, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { ModalDirective } from "ng2-bootstrap";

@Component({
  moduleId: module.id,
  selector: "modal-child",
  templateUrl: "childModal.component.html",
  exportAs: "childModal"
})
export class ChildModalComponent {
  @ViewChild("childModal") public childModal: ModalDirective;

  @Input()
  modal = {
    "header": "MODAL HEADER",
    "text": "MODAL TEXT",
    "txtAceptar": "Eliminar",
    "txtCancelar": "Cancelar"
  };
  @Output()
  ok: EventEmitter<any> = new EventEmitter();


  public showChildModal(): void {
    this.childModal.show();
  }

  public hideChildModal(): void {
    this.childModal.hide();
    this.ok.emit(false);
  }

  public confirChildModal(): void {
    this.childModal.hide();
    this.ok.emit(true);
  }
}
