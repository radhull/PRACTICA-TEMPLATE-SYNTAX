import {
    Component, Input, Output, OnInit, EventEmitter,
    trigger,
    state,
    style,
    animate,
    transition
} from "@angular/core";

import { Person } from "../person.model";
import { PeopleService } from "../servicio/people.service";
// import { NotificationService } from "./notification.service";

// import { ConfigService } from "../shared/utils/config.service";
// import { HighlightDirective } from "../highlight.directive";

// import { ModalDirective } from "ng2-bootstrap";

@Component({
    moduleId: module.id,
    selector: "person-card",
    templateUrl: "person-card.component.html",
    animations: [
        trigger("flyInOut", [
            state("in", style({ opacity: 1, transform: "translateX(0)" })),
            transition("void => *", [
                style({
                    opacity: 0,
                    transform: "translateX(-100%)"
                }),
                animate("0.5s ease-in")
            ]),
            transition("* => void", [
                animate("0.2s 10 ease-out", style({
                    opacity: 0,
                    transform: "translateX(100%)"
                }))
            ])
        ])
    ]
})
export class PersonCardComponent implements OnInit {

    // @ViewChild("childModal")
    // public childModal: ModalDirective;

    @Input()
    obj: Person;

    @Output() removePerson = new EventEmitter();
    @Output() personCreated = new EventEmitter();

    edittedPerson: Person;
    onEdit: boolean = false;



    // // Modal properties
    // @ViewChild("modal")
    // modal: any;
    // items: string[] = ["item1", "item2", "item3"];
    // selected: string;
    // output: string;
    // index: number = 0;
    // backdropOptions = [true, false, "static"];
    // animation: boolean = true;
    // keyboard: boolean = true;
    // backdrop: string | boolean = true;

    constructor(private peopleService: PeopleService) { }

    ngOnInit() {

        this.edittedPerson = this.obj;
        if (this.obj._id === "-1") {
            this.editPerson();
        }
    }

    editPerson() {
        this.onEdit = !this.onEdit;
        this.edittedPerson = this.obj;

    }

    createPerson() {

        this.peopleService.insertPerson(this.edittedPerson)
            .then(() => {
                this.obj = this.edittedPerson;
                this.edittedPerson = this.obj;
                this.onEdit = false;
                this.personCreated.emit({ value: this.personCreated });
            })
            .catch((error) => {
                // this.notificationService.printErrorMessage("Failed to created user");
                // this.notificationService.printErrorMessage(error);
            });


    }

    updatePerson() {

        this.peopleService.savePerson(this.edittedPerson)
            .then(() => {
                this.obj = this.edittedPerson;
                this.onEdit = !this.onEdit;
                // this.notificationService.printSuccessMessage(this.obj.name + " has been updated");
                // this.slimLoader.complete();
            }).catch((error) => {
                // this.notificationService.printErrorMessage("Failed to edit user");
                // this.notificationService.printErrorMessage(error);
                // this.slimLoader.complete();
            });
    }

    openRemoveModal() {
        // this.notificationService.openConfirmationDialog("Are you sure you want to remove "
        //     + this.obj.name + "?",
        //     () => {

        // this.peopleService.deletePerson(this.obj._id)
        //     .subscribe(
        //     res => {
        //         this.removePerson.emit({
        //             value: this.obj
        //         });

        //     }, error => {
        //         this.notificationService.printErrorMessage(error);

        //     })
        // });
    }


    // public hideChildModal(): void {
    //     this.childModal.hide();
    // }

    opened() {

    }

    isPersonValid(): boolean {
        return !(this.edittedPerson.name === "")
            && !(this.edittedPerson.balance === "");
    }

}