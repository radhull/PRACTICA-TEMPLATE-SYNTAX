import { Component } from "@angular/core";

@Component({
  moduleId: module.id,
  selector: "person",
  template: `
    <div class="container-fluid">
      <h2>Marvel Heroes</h2>
      <router-outlet></router-outlet>
    </div>
  `
})
export class PersonComponent {

}
