import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { BrowserModule, Title } from "@angular/platform-browser";

import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { ButtonsModule, ModalModule, TooltipModule } from "ng2-bootstrap";



import { MODULE_COMPONENTS, MODULE_ROUTES, MODULE_PROVIDERS } from "./person.routes";








@NgModule({
    imports: [BrowserModule, FormsModule, CommonModule, ButtonsModule.forRoot(), ModalModule.forRoot(), TooltipModule.forRoot(),

        RouterModule.forChild(MODULE_ROUTES)],
    declarations: [MODULE_COMPONENTS],
    providers: [MODULE_PROVIDERS, Title]
})
export class PersonModule { }