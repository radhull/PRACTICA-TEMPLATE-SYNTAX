import { Route } from "@angular/router";

import { PeopleComponent } from "./people/people.component";
import { PersonRowComponent } from "./person-row/person-row.component";
import { PersonCardComponent } from "./person-row/person-card.component";
import { PersonEditComponent } from "./person-edit/person-edit.component";
import { ChildModalComponent } from "./modal/childModal.component";
import { GenderEditorComponent } from "./gender-edit/gender-editor.component";

import { DefaultImage } from "./defaultImage.directive";
import { HighlightDirective } from "./highlight.directive";

import { PeopleService } from "./servicio/people.service";
// import { NotificationService } from "./person-row/notification.service";

export const MODULE_ROUTES: Route[] = [
  { path: "people", component: PeopleComponent },
  { path: "", redirectTo: "/people", pathMatch: "full" }
];


export const MODULE_COMPONENTS = [PeopleComponent, PersonRowComponent, PersonEditComponent, PersonCardComponent,
  ChildModalComponent, GenderEditorComponent, DefaultImage, HighlightDirective];


export const MODULE_PROVIDERS = [PeopleService];
