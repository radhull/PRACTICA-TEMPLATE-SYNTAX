import { Person } from "../person.model";

export const PEOPLE: Person[] = [
    {
        "_id": "58945d226107f9435e76d438",
        "index": 0,
        "guid": "8504d0d6-bda9-445e-a8b8-42ac89517eb7",
        "isActive": false,
        "balance": "$2,777.76",
        "picture": "http://placehold.it/32x32",
        "age": 21,
        "eyeColor": "blue",
        "name": "Wooten Sims",
        "gender": "male",
        "company": "SENMAO",
        "email": "wootensims@senmao.com",
        "phone": "+1 (802) 547-3453",
        "address": "591 Mill Street, Boling, Connecticut, 7657",
        // tslint:disable-next-line:max-line-length
        "about": "Aliqua velit non sunt irure. Aliqua adipisicing anim nisi id officia officia incididunt. Proident excepteur ex mollit veniam proident ad in pariatur ullamco voluptate. Commodo pariatur tempor irure aliqua do. Laboris et laboris amet culpa. Sunt nulla labore pariatur exercitation fugiat cillum tempor consectetur et tempor proident. Officia fugiat duis adipisicing sit enim ut aute commodo est sint amet.\r\n",
        "registered": "2016-10-06T10:16:55 -02:00",
        "latitude": 62.528836,
        "longitude": 58.52817,
        "tags": [
            "commodo",
            "dolore",
            "est",
            "officia",
            "aliquip",
            "eiusmod",
            "aliquip"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Sawyer Hutchinson"
            },
            {
                "id": 1,
                "name": "Marcie Hyde"
            },
            {
                "id": 2,
                "name": "Wynn Hester"
            }
        ],
        "greeting": "Hello, Wooten Sims! You have 9 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "58945d220fffd0967b4e5616",
        "index": 1,
        "guid": "9045b945-1091-4dc7-aacd-41a2d1aba7cb",
        "isActive": true,
        "balance": "$1,153.87",
        "picture": "http://placehold.it/32x32",
        "age": 21,
        "eyeColor": "blue",
        "name": "Beard Cooke",
        "gender": "male",
        "company": "QUILITY",
        "email": "beardcooke@quility.com",
        "phone": "+1 (904) 484-2361",
        "address": "233 Norman Avenue, Grazierville, Wisconsin, 8614",
        // tslint:disable-next-line:max-line-length
        "about": "Adipisicing eu cillum fugiat exercitation irure ullamco Lorem qui do enim. Quis labore ut ad ea officia veniam cupidatat quis excepteur pariatur aliqua sunt. Eu Lorem duis in ut sunt velit duis do id.\r\n",
        "registered": "2015-02-18T01:43:05 -01:00",
        "latitude": 16.201536,
        "longitude": -83.240311,
        "tags": [
            "aliquip",
            "consectetur",
            "amet",
            "non",
            "non",
            "ipsum",
            "adipisicing"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Berta Durham"
            },
            {
                "id": 1,
                "name": "Luella Jennings"
            },
            {
                "id": 2,
                "name": "Grant Russell"
            }
        ],
        "greeting": "Hello, Beard Cooke! You have 8 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "58945d221e151f7efd343413",
        "index": 2,
        "guid": "daa51e32-6e3c-48b3-9163-c2fe0a424b86",
        "isActive": true,
        "balance": "$1,761.89",
        "picture": "http://placehold.it/32x32",
        "age": 30,
        "eyeColor": "brown",
        "name": "Moody Roberson",
        "gender": "male",
        "company": "COGENTRY",
        "email": "moodyroberson@cogentry.com",
        "phone": "+1 (996) 500-2058",
        "address": "289 Matthews Place, Woodlands, Hawaii, 6948",
        // tslint:disable-next-line:max-line-length
        "about": "Amet laborum consequat eu fugiat. Adipisicing aute non duis aliquip. Amet minim minim et eiusmod aliqua est laboris aliqua adipisicing sint sint non velit consequat. Amet velit culpa ad eu voluptate adipisicing tempor ad duis et culpa ea cupidatat. Aute aliquip velit incididunt velit magna ea. Ipsum cupidatat anim occaecat deserunt incididunt dolore est veniam fugiat non ipsum adipisicing eu sunt.\r\n",
        "registered": "2016-02-20T08:57:51 -01:00",
        "latitude": -11.193937,
        "longitude": 47.081694,
        "tags": [
            "dolore",
            "nisi",
            "ut",
            "amet",
            "veniam",
            "nostrud",
            "exercitation"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Millicent Gray"
            },
            {
                "id": 1,
                "name": "Jeannine Lang"
            },
            {
                "id": 2,
                "name": "Beulah Macdonald"
            }
        ],
        "greeting": "Hello, Moody Roberson! You have 6 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "58945d2298e348b386b8c592",
        "index": 3,
        "guid": "1c0b9f71-dde5-497a-ab23-7d2253e966ff",
        "isActive": true,
        "balance": "$3,399.43",
        "picture": "http://placehold.it/32x32",
        "age": 37,
        "eyeColor": "blue",
        "name": "Atkins Odonnell",
        "gender": "male",
        "company": "HOMELUX",
        "email": "atkinsodonnell@homelux.com",
        "phone": "+1 (873) 466-3302",
        "address": "170 Pulaski Street, Spokane, Maine, 2184",
        // tslint:disable-next-line:max-line-length
        "about": "Irure fugiat amet mollit velit eu. Nisi elit reprehenderit duis labore in tempor ex ipsum elit voluptate elit amet. Non reprehenderit deserunt aliquip consectetur aute. Dolore id laborum nulla tempor excepteur est est id commodo commodo aute. Eiusmod enim esse ex nostrud dolor velit aliquip nisi. Dolore pariatur cupidatat fugiat sunt est nulla mollit incididunt magna tempor.\r\n",
        "registered": "2015-08-24T06:17:40 -02:00",
        "latitude": 31.688798,
        "longitude": 69.044161,
        "tags": [
            "sunt",
            "consectetur",
            "dolor",
            "consectetur",
            "esse",
            "cillum",
            "elit"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Alberta Carey"
            },
            {
                "id": 1,
                "name": "Jimmie Hardin"
            },
            {
                "id": 2,
                "name": "Bartlett Nunez"
            }
        ],
        "greeting": "Hello, Atkins Odonnell! You have 1 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "58945d228a487bba597dae7d",
        "index": 4,
        "guid": "af3f8743-5fd1-4a70-8db2-0b5e11e35004",
        "isActive": false,
        "balance": "$3,645.99",
        "picture": "http://placehold.it/32x32",
        "age": 39,
        "eyeColor": "brown",
        "name": "Cooke Gross",
        "gender": "male",
        "company": "COMVEX",
        "email": "cookegross@comvex.com",
        "phone": "+1 (959) 481-2026",
        "address": "763 Hutchinson Court, Kiskimere, West Virginia, 8767",
        // tslint:disable-next-line:max-line-length
        "about": "Aliquip sunt enim velit mollit Lorem consequat aliqua. Officia nulla aliqua officia aliquip incididunt cupidatat consequat amet nulla mollit nulla ex adipisicing ipsum. Est qui cillum fugiat ex nisi qui aliqua. Consequat id veniam nisi commodo ex ut. Et adipisicing nulla exercitation consequat. Labore duis ad enim reprehenderit dolore aliquip. Deserunt magna voluptate exercitation sunt ex ea aliqua.\r\n",
        "registered": "2015-05-27T07:34:30 -02:00",
        "latitude": -48.608317,
        "longitude": -26.654383,
        "tags": [
            "velit",
            "elit",
            "ut",
            "duis",
            "pariatur",
            "veniam",
            "laborum"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Cassandra Orr"
            },
            {
                "id": 1,
                "name": "Dillard Rocha"
            },
            {
                "id": 2,
                "name": "Juarez Rivas"
            }
        ],
        "greeting": "Hello, Cooke Gross! You have 2 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "58945d226a7e975000a118b8",
        "index": 5,
        "guid": "9ffc5a56-c430-41ce-9889-541959fa3d15",
        "isActive": false,
        "balance": "$1,465.03",
        "picture": "http://placehold.it/32x32",
        "age": 23,
        "eyeColor": "brown",
        "name": "Leslie Maxwell",
        "gender": "female",
        "company": "SEALOUD",
        "email": "lesliemaxwell@sealoud.com",
        "phone": "+1 (927) 423-3545",
        "address": "998 Bragg Street, Boyd, Kentucky, 9134",
        // tslint:disable-next-line:max-line-length
        "about": "Anim id qui laborum amet commodo velit do. Veniam duis do voluptate reprehenderit eiusmod ipsum dolor ad Lorem aliqua. Dolor nisi aute laborum fugiat aliqua do ea officia aliqua sit. Aliquip ullamco cupidatat magna tempor laboris duis. Irure deserunt minim non culpa amet culpa id cillum labore non dolor sit pariatur. Qui enim excepteur reprehenderit tempor eiusmod Lorem sunt ullamco.\r\n",
        "registered": "2014-03-19T08:36:25 -01:00",
        "latitude": -38.43947,
        "longitude": 2.363059,
        "tags": [
            "ullamco",
            "quis",
            "laborum",
            "labore",
            "quis",
            "in",
            "do"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Hanson Nielsen"
            },
            {
                "id": 1,
                "name": "Florine Vance"
            },
            {
                "id": 2,
                "name": "Phyllis Slater"
            }
        ],
        "greeting": "Hello, Leslie Maxwell! You have 6 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "58945d22959db7623789483f",
        "index": 6,
        "guid": "7c3f2f20-57da-42f3-b829-6e7ca573fe81",
        "isActive": true,
        "balance": "$3,995.58",
        "picture": "http://placehold.it/32x32",
        "age": 24,
        "eyeColor": "blue",
        "name": "Rodriguez Leblanc",
        "gender": "male",
        "company": "GOLOGY",
        "email": "rodriguezleblanc@gology.com",
        "phone": "+1 (969) 565-3652",
        "address": "449 Jamaica Avenue, Trail, Georgia, 2997",
        // tslint:disable-next-line:max-line-length
        "about": "In ullamco in ad non dolor sunt. Enim nulla deserunt laborum pariatur nostrud dolor. Commodo laborum laborum eiusmod duis excepteur ea qui exercitation labore ullamco. Sit ex nostrud nostrud anim amet pariatur minim ut enim commodo qui quis. Incididunt consectetur reprehenderit eiusmod pariatur Lorem sit anim quis sint esse laborum elit. Et dolor sit ullamco occaecat ad do. Eu sit incididunt quis dolor ex dolore officia.\r\n",
        "registered": "2015-04-10T06:56:21 -02:00",
        "latitude": 40.641242,
        "longitude": -115.538466,
        "tags": [
            "ut",
            "tempor",
            "incididunt",
            "duis",
            "do",
            "enim",
            "ad"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Lara Davis"
            },
            {
                "id": 1,
                "name": "Reilly Bird"
            },
            {
                "id": 2,
                "name": "Lessie Harrington"
            }
        ],
        "greeting": "Hello, Rodriguez Leblanc! You have 3 unread messages.",
        "favoriteFruit": "banana"
    },
    {
        "_id": "58945d2201f27dc0a8916912",
        "index": 7,
        "guid": "297cf7ad-dce5-4dbc-8e2d-16c34d4f0fce",
        "isActive": false,
        "balance": "$2,336.79",
        "picture": "http://placehold.it/32x32",
        "age": 22,
        "eyeColor": "brown",
        "name": "Ernestine Battle",
        "gender": "female",
        "company": "LIQUICOM",
        "email": "ernestinebattle@liquicom.com",
        "phone": "+1 (953) 433-2865",
        "address": "203 Hopkins Street, Bentley, Arizona, 8938",
        // tslint:disable-next-line:max-line-length
        "about": "Incididunt nisi incididunt enim nostrud ad dolor occaecat duis consequat ea. Minim eiusmod fugiat proident commodo commodo exercitation. Proident dolor aliqua occaecat veniam irure voluptate esse ea tempor id ut eu cillum. Deserunt velit qui eu aute aliquip proident ullamco. Ipsum in velit sit adipisicing sint velit velit occaecat exercitation ea est. Sunt esse elit proident nulla et eiusmod culpa aliqua occaecat consectetur excepteur. Cupidatat veniam sit sunt est anim veniam officia est ipsum.\r\n",
        "registered": "2016-12-17T09:34:55 -01:00",
        "latitude": -0.252966,
        "longitude": 83.338586,
        "tags": [
            "magna",
            "nostrud",
            "ad",
            "id",
            "ut",
            "dolore",
            "veniam"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Carey Payne"
            },
            {
                "id": 1,
                "name": "Brittney Warren"
            },
            {
                "id": 2,
                "name": "Benjamin Henson"
            }
        ],
        "greeting": "Hello, Ernestine Battle! You have 5 unread messages.",
        "favoriteFruit": "apple"
    },
    {
        "_id": "58945d222eb00b4bf48dec5f",
        "index": 8,
        "guid": "f3003e7e-ff18-4d5d-ab21-c9510e6daf28",
        "isActive": false,
        "balance": "$1,173.94",
        "picture": "http://placehold.it/32x32",
        "age": 29,
        "eyeColor": "green",
        "name": "Mable Maldonado",
        "gender": "female",
        "company": "ZANITY",
        "email": "mablemaldonado@zanity.com",
        "phone": "+1 (829) 553-2238",
        "address": "996 Fair Street, Saranap, District Of Columbia, 2933",
        // tslint:disable-next-line:max-line-length
        "about": "Ullamco anim cillum consequat ad officia ut duis. Sit quis ut et do eiusmod. Dolor incididunt ut non aute culpa dolor sint sint labore. Veniam occaecat dolor aliquip quis proident culpa pariatur id veniam voluptate laboris est minim do. Quis cillum velit amet pariatur est id proident. Cillum incididunt incididunt proident magna occaecat mollit.\r\n",
        "registered": "2017-01-26T07:57:07 -01:00",
        "latitude": -38.976188,
        "longitude": -32.70006,
        "tags": [
            "dolore",
            "magna",
            "est",
            "ut",
            "consequat",
            "ea",
            "pariatur"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Weeks Greene"
            },
            {
                "id": 1,
                "name": "Beth Giles"
            },
            {
                "id": 2,
                "name": "Sherri Woods"
            }
        ],
        "greeting": "Hello, Mable Maldonado! You have 4 unread messages.",
        "favoriteFruit": "strawberry"
    },
    {
        "_id": "58945d22a03dda690e5aa372",
        "index": 9,
        "guid": "456c2c83-4cbb-4323-b816-a832cb525e14",
        "isActive": true,
        "balance": "$1,022.97",
        "picture": "http://placehold.it/32x32",
        "age": 37,
        "eyeColor": "brown",
        "name": "Park Duncan",
        "gender": "male",
        "company": "TALKALOT",
        "email": "parkduncan@talkalot.com",
        "phone": "+1 (875) 587-2008",
        "address": "434 Kingsway Place, Berwind, New York, 1040",
        // tslint:disable-next-line:max-line-length
        "about": "Qui aute est aliquip occaecat ipsum ullamco culpa ex esse elit ullamco aliqua. Mollit aliquip sit laborum sunt enim ad minim exercitation enim dolor ut in. Aliqua labore nisi dolore mollit quis labore. Ut Lorem dolore occaecat ad excepteur id in eiusmod anim anim magna ipsum aliquip ullamco. Est aliqua Lorem cupidatat excepteur mollit enim laborum sit Lorem proident dolor nisi veniam. Dolor dolore eu sunt voluptate esse nulla. Aliqua id dolore sit nostrud tempor et voluptate commodo ut adipisicing eiusmod labore ex.\r\n",
        "registered": "2015-01-19T10:00:46 -01:00",
        "latitude": -29.990844,
        "longitude": -116.864633,
        "tags": [
            "mollit",
            "aute",
            "culpa",
            "ullamco",
            "Lorem",
            "cillum",
            "aliquip"
        ],
        "friends": [
            {
                "id": 0,
                "name": "Kelly Bowman"
            },
            {
                "id": 1,
                "name": "Cochran Dunn"
            },
            {
                "id": 2,
                "name": "Page Herrera"
            }
        ],
        "greeting": "Hello, Park Duncan! You have 2 unread messages.",
        "favoriteFruit": "apple"
    }
];
