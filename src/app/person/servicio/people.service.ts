import { Injectable } from "@angular/core";

import { Person, ReducedPerson } from "../person.model";
import { PEOPLE } from "./people.mock";

@Injectable()
export class PeopleService {

    private clonePerson(person: Person): Person {
        const clone = Object.assign({}, person);
        if (person.tags) {
            clone.tags = [...person.tags];
        }
        if (person.friends) {
            clone.friends = [];
            for (let friend of person.friends) {
                clone.friends.push(Object.assign({}, friend));
            }
        }
        return clone;
    }

    private delayedPromise<T>(obj: T, delay?: number): Promise<T> {
        if (delay === -1) {
            return Promise.resolve(obj);
        }
        return new Promise<T>((resolve) => {
            setTimeout(() => {
                resolve(obj);
            }, delay || 200);
        });
    }

    private reducePerson(person: Person): ReducedPerson {
        return {
            _id: person._id,
            isActive: person.isActive,
            balance: person.balance,
            picture: person.picture,
            age: person.age,
            eyeColor: person.eyeColor,
            name: person.name,
            gender: person.gender,
            registered: person.registered
        };
    }

    getPeople(skip = 0, count = 5): Promise<ReducedPerson[]> {
        let result = PEOPLE;
        if (skip) {
            result = result.slice(skip);
        }
        if (count) {
            result = result.slice(0, count);
        }

        return this.delayedPromise(result.map((person) => {
            return this.reducePerson(person);
        }));
    }

    getPerson(id: string): Promise<Person> {
        return this.delayedPromise(this.clonePerson(PEOPLE.find((p) => p._id === id)));
    }

    savePerson(person: Person): Promise<Person> {
        const index = PEOPLE.findIndex((p) => p._id === person._id);
        if (index >= 0) {
            PEOPLE.splice(index, 1, person);
        }
        return this.delayedPromise(person);
    }

    deletePerson(personOrId: Person | string): Promise<Person | string> {
        const id = typeof personOrId === "string" ? personOrId : personOrId._id;

        const index = PEOPLE.findIndex((p) => p._id === id);
        if (index >= 0) {
            PEOPLE.splice(index, 1);
        }

        return this.delayedPromise(personOrId);
    }

    insertPerson(person: Person): Promise<Person> {
        PEOPLE.push(person);
        return this.delayedPromise(person);
    }
}
